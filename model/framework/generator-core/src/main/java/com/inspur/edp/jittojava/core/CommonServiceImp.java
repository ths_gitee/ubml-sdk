/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.jittojava.core;

import com.inspur.edp.jittojava.context.service.CommonService;
import com.inspur.edp.lcm.metadata.api.service.FileService;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import com.inspur.edp.lcm.metadata.core.MetadataProjectServiceImp;
import com.inspur.edp.lcm.metadata.devcommon.ManagerUtils;
import java.util.List;

/**
 * Classname CommonServiceImp Description 实现类 Date 2019/8/8 16:42
 *
 * @author zhongchq
 * @version 1.0
 */
public class CommonServiceImp implements CommonService {

    FileService fileService = new FileServiceImp();

    MetadataProjectService metadataProjectService = new MetadataProjectServiceImp();

    CommonServiceCore commonServiceCore = new CommonServiceCore();

    /**
     * @param proPath 相对路径
     * @return java.lang.String
     * @throws
     * @author zhongchq
     **/
    @Override
    public String getProjectPath(String proPath) {
        String javaPath = fileService.getCombinePath(metadataProjectService.getProjPath(proPath), "java");
        String absJavaPath = ManagerUtils.getAbsolutePath(javaPath);

        return commonServiceCore.getProjectPath(absJavaPath);

    }

    @Override
    public List<String> getJarPath(String proPath) {
        String javaPath = fileService.getCombinePath(metadataProjectService.getProjPath(proPath), "java");
        String absJavaPath = ManagerUtils.getAbsolutePath(javaPath);
        return commonServiceCore.getJarPath(absJavaPath);
    }
}
