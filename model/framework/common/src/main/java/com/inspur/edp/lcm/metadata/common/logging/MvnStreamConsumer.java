/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.logging;

import com.inspur.lcm.metadata.logging.LoggerDisruptorQueue;
import io.iec.edp.caf.boot.context.CAFContext;
import org.apache.maven.shared.invoker.InvocationOutputHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classname MvnStreamConsumer Description 获取日志 Date 2020/3/4 11:32
 *
 * @author zhongchq
 * @version 1.0
 */
public class MvnStreamConsumer implements InvocationOutputHandler {
    private static final Logger log = LoggerFactory.getLogger(MvnStreamConsumer.class);

    private String exceptionString;

    @Override
    public void consumeLine(String line) {
        if (line.contains("[INFO]")) {
            LoggerDisruptorQueue.publishEvent(line, CAFContext.current.getUserId());
            log.info(line);
        } else if (line.contains("[WARNING]")) {
            LoggerDisruptorQueue.publishEvent(line, CAFContext.current.getUserId());
            log.warn(line);
        } else if (line.contains("[ERROR]")) {
            this.exceptionString += line + "\r\n";
            LoggerDisruptorQueue.publishEvent(line, CAFContext.current.getUserId());
            log.error(line);
        }
    }

    public String getExceptionString() {
        return exceptionString;
    }

    public void setExceptionString(String exceptionString) {
        this.exceptionString = exceptionString;
    }
}
