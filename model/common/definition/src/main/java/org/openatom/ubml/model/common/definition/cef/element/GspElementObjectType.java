/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.element;

/**
 * The Definition Of ElementObjectType
 *
 * @ClassName: ElementObjectType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum GspElementObjectType {
    /**
     * 未设置
     */
    None(0),
    /**
     * 关联
     */
    Association(1),
    /**
     * 枚举
     */
    Enum(2),
    /**
     * 动态属性
     */
    DynamicProp(3);

    private static java.util.HashMap<Integer, GspElementObjectType> mappings;
    private int intValue;

    private GspElementObjectType(int value) {
        intValue = value;
        GspElementObjectType.getMappings().put(value, this);
    }

    public synchronized static java.util.HashMap<Integer, GspElementObjectType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, GspElementObjectType>();
        }
        return mappings;
    }

    public static GspElementObjectType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}