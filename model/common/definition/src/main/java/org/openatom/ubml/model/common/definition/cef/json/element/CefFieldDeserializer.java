/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.util.ArrayList;
import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.cef.collection.GspAssociationCollection;
import org.openatom.ubml.model.common.definition.cef.collection.GspEnumValueCollection;
import org.openatom.ubml.model.common.definition.cef.collection.GspFieldCollection;
import org.openatom.ubml.model.common.definition.cef.element.ElementDefaultVauleType;
import org.openatom.ubml.model.common.definition.cef.element.EnumIndexType;
import org.openatom.ubml.model.common.definition.cef.element.FieldCollectionType;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
import org.openatom.ubml.model.common.definition.cef.element.GspElementDataType;
import org.openatom.ubml.model.common.definition.cef.element.GspElementObjectType;
import org.openatom.ubml.model.common.definition.cef.entity.CustomizationInfo;
import org.openatom.ubml.model.common.definition.cef.entity.DynamicPropSetInfo;
import org.openatom.ubml.model.common.definition.cef.entity.GspCommonField;
import org.openatom.ubml.model.common.definition.cef.entity.MappingInfo;
import org.openatom.ubml.model.common.definition.cef.entity.MappingRelation;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Json Parser Of CefField
 *
 * @ClassName: CefFieldDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class CefFieldDeserializer extends JsonDeserializer<IGspCommonField> {

    @Override
    public IGspCommonField deserialize(JsonParser jsonParser,
                                       DeserializationContext deserializationContext) {
        return deserializeCommonField(jsonParser);
    }

    public IGspCommonField deserializeCommonField(JsonParser jsonParser) {
        GspCommonField field = CreateField();

        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(field, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);
        return field;
    }

    private void readPropertyValue(GspCommonField field, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CefNames.ID:
                field.setID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.CODE:
                field.setCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.NAME:
                field.setName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.LABEL_ID:
                field.setLabelID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.BE_LABEL:
                field.setBeLabel(SerializerUtils.readStringArray(jsonParser));
                break;
            case CefNames.BIZ_TAG_IDS:
                field.setBizTagIds(SerializerUtils.readStringArray(jsonParser));
                break;
            case CefNames.MDATA_TYPE:
                field.setMDataType(SerializerUtils
                        .readPropertyValue_Enum(jsonParser, GspElementDataType.class,
                                GspElementDataType.values()));
                break;
            case CefNames.DEFAULT_VALUE:
                field.setDefaultValue(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.DEFAULT_VAULE_TYPE:
            case CefNames.DEFAULT_VALUE_TYPE:
                field.setDefaultValueType(SerializerUtils
                        .readPropertyValue_Enum(jsonParser, ElementDefaultVauleType.class,
                                ElementDefaultVauleType.values()));
                break;
            case CefNames.LENGTH:
                field.setLength(SerializerUtils.readPropertyValue_Integer(jsonParser));
                break;
            case CefNames.PRECISION:
                field.setPrecision(SerializerUtils.readPropertyValue_Integer(jsonParser));
                break;
            case CefNames.OBJECT_TYPE:
                field.setObjectType(SerializerUtils
                        .readPropertyValue_Enum(jsonParser, GspElementObjectType.class,
                                GspElementObjectType.values()));
                break;
            case CefNames.CHILD_ASSOCIATIONS:
                readChildAssociations(jsonParser, field);
                break;
            case CefNames.CONTAIN_ENUM_VALUES:
                readContainEnumValues(jsonParser, field);
                break;
            case CefNames.IS_VIRTUAL:
                field.setIsVirtual(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.IS_REQUIRE:
                field.setIsRequire(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.IS_REF_ELEMENT:
                field.setIsRefElement(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.IS_MULTI_LANGUAGE:
                field.setIsMultiLanguage(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.IS_REF:
                field.setIsRef(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.REF_ELEMENT_ID:
                field.setRefElementId(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.IS_UDT:
                field.setIsUdt(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.UDT_PKG_NAME:
                field.setUdtPkgName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.UDT_ID:
                field.setUdtID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.UDT_NAME:
                field.setUdtName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.MAPPING_RELATION:
                readMappingRelation(jsonParser, field);
                break;
            case CefNames.CHILD_ELEMENTS:
                readChildElements(jsonParser, field);
                break;
            case CefNames.COLLECTION_TYPE:
                field.setCollectionType(SerializerUtils
                        .readPropertyValue_Enum(jsonParser, FieldCollectionType.class,
                                FieldCollectionType.values()));
                break;
            case CefNames.IS_FROM_ASSO_UDT:
                field.setIsFromAssoUdt(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.I18N_RESOURCE_INFO_PREFIX:
                field.setI18nResourceInfoPrefix(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.CUSTOMIZATION_INFO:
                field.setCustomizationInfo(
                        SerializerUtils.readPropertyValue_Object(CustomizationInfo.class, jsonParser));
                try {
                    jsonParser.nextToken();
                } catch (IOException e) {
                    throw new RuntimeException(
                            String.format("GspCommonDataTypeDeserializer反序列化错误：%1$s", propName));
                }
                break;
            case CefNames.DYNAMIC_PROP_SET_INFO:
                readDynamicPropSetInfo(jsonParser, field);
                break;
            case CefNames.ENUM_INDEX_TYPE:
                field.setEnumIndexType(SerializerUtils
                        .readPropertyValue_Enum(jsonParser, EnumIndexType.class, EnumIndexType.values()));
                break;
            case CefNames.ENABLE_RTRIM:
                field.setEnableRtrim(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.IS_BIG_NUMBER:
                field.setIsBigNumber(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            default:
                if (!readExtendFieldProperty(field, propName, jsonParser)) {
                    throw new RuntimeException(String.format("CefFieldDeserializer未识别的属性名：%1$s", propName));
                }
        }
    }

    protected void readDynamicPropSetInfo(JsonParser jsonParser, GspCommonField field) {
        JsonDeserializer<DynamicPropSetInfo> deserializer = new DynamicPropSetInfoDeserializer();
        try {
            DynamicPropSetInfo value = deserializer.deserialize(jsonParser, null);
            field.setDynamicPropSetInfo(value);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void readMappingRelation(JsonParser jsonParser, GspCommonField field) {
        MappingRelation mappingInfos = new MappingRelation();
        ArrayList<MappingInfo> list = new ArrayList<MappingInfo>();
        JsonDeserializer<MappingInfo> deserializer = new MappingInfoDeserializer();
        SerializerUtils.readArray(jsonParser, deserializer, list);
        for (MappingInfo info : list) {
            mappingInfos.add(info);
        }
        field.setMappingRelation(mappingInfos);
    }

    protected void readChildElements(JsonParser jsonParser, GspCommonField field) {
        GspFieldCollection collection = CreateFieldCollection();
        SerializerUtils.readArray(jsonParser, this, collection);
        field.setChildElements(collection);
    }

    protected void readContainEnumValues(JsonParser jsonParser, GspCommonField field) {
        GspEnumValueCollection collection = new GspEnumValueCollection();
        GspEnumValueDeserializer deserializer = new GspEnumValueDeserializer();
        SerializerUtils.readArray(jsonParser, deserializer, collection);
        field.setContainEnumValues(collection);
    }

    private void readChildAssociations(JsonParser jsonParser, GspCommonField field) {
        GspAssociationCollection collection = CreateGspAssoCollection();
        GspAssociationDeserializer deserializer = CreateGspAssoDeserializer();
        SerializerUtils.readArray(jsonParser, deserializer, collection);
        handleChildAssociations(collection, field);
        field.setChildAssociations(collection);
    }

    private void handleChildAssociations(GspAssociationCollection associations,
                                         GspCommonField field) {
        for (GspAssociation association : associations) {
            association.setBelongElement(field);
        }
    }

    protected abstract GspFieldCollection CreateFieldCollection();

    protected abstract GspCommonField CreateField();

    protected GspAssociationCollection CreateGspAssoCollection() {
        return new GspAssociationCollection();
    }

    protected GspAssociationDeserializer CreateGspAssoDeserializer() {
        return new GspAssociationDeserializer(this);
    }

    protected boolean readExtendFieldProperty(GspCommonField field, String propName,
                                              JsonParser jsonParser) {
        return false;
    }
}
