/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import java.util.HashMap;
import org.openatom.ubml.model.common.definition.cef.collection.ValElementCollection;

/**
 * The  Josn Deserializer Of Validation Request Child  Elements
 *
 * @ClassName: ValRequestChildElementsDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ValRequestChildElementsDeserializer extends RequestChildElementsDeserializer<ValElementCollection> {

    @Override
    HashMap<String, ValElementCollection> createHashMap() {
        return new HashMap<String, ValElementCollection>();
    }

    @Override
    ValElementCollection createValueType() {
        return new ValElementCollection();
    }
}
