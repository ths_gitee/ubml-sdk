/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.util.EnumSet;
import org.openatom.ubml.model.be.definition.beenum.BETriggerTimePointType;
import org.openatom.ubml.model.be.definition.beenum.RequestNodeTriggerType;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.common.definition.cef.entity.CustomizationInfo;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.json.CommonModelNames;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class BizOperationDeserializer<T extends BizOperation> extends JsonDeserializer<T> {
    public static EnumSet<RequestNodeTriggerType> readRequestNodeTriggerType(JsonParser jsonParser) {
        EnumSet<RequestNodeTriggerType> result = EnumSet.noneOf(RequestNodeTriggerType.class);
        int intValueSum = SerializerUtils.readPropertyValue_Integer(jsonParser);
        RequestNodeTriggerType[] values = RequestNodeTriggerType.values();
        // 不包含none
        for (int i = values.length - 1; i > 0; i--) {
            RequestNodeTriggerType value = values[i];
            if (intValueSum > 0 && intValueSum >= value.getValue()) {
                result.add(value);
                intValueSum -= value.getValue();
            }
        }
        return result;
    }

    public static EnumSet<BETriggerTimePointType> readBETriggerTimePointType(JsonParser jsonParser) {
        EnumSet<BETriggerTimePointType> result = EnumSet.noneOf(BETriggerTimePointType.class);
        int intValueSum = SerializerUtils.readPropertyValue_Integer(jsonParser);
        BETriggerTimePointType[] values = BETriggerTimePointType.values();
        // 不包含none
        for (int i = values.length - 1; i > 0; i--) {
            BETriggerTimePointType value = values[i];
            if (intValueSum > 0 && intValueSum >= value.getValue()) {
                result.add(value);
                intValueSum -= value.getValue();
            }
        }
        return result;
    }

    @Override
    public final T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        return deserializeBizOperation(jsonParser);
    }

    private final T deserializeBizOperation(JsonParser jsonParser) {
        T op = createBizOp();

        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(op, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);

        return op;
    }

    private void readPropertyValue(BizOperation op, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CommonModelNames.ID:
                op.setID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CommonModelNames.CODE:
                op.setCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CommonModelNames.NAME:
                op.setName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.DESCRIPTION:
                op.setDescription(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.COMPONENT_ID:
                op.setComponentId(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.COMPONENT_NAME:
                op.setComponentName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.COMPONENT_PKG_NAME:
                op.setComponentPkgName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.IS_VISIBLE:
                op.setIsVisible(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case BizEntityJsonConst.OP_TYPE:
                SerializerUtils.readPropertyValue_String(jsonParser);
                break;
            case BizEntityJsonConst.BELONG_MODEL_ID:
                op.setBelongModelID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.IS_REF:
                op.setIsRef(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case BizEntityJsonConst.IS_GENERATE_COMPONENT:
                op.setIsGenerateComponent(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.CUSTOMIZATION_INFO:
                op.setCustomizationInfo(SerializerUtils.readPropertyValue_Object(CustomizationInfo.class, jsonParser));
                try {
                    jsonParser.nextToken();
                } catch (IOException e) {
                    throw new RuntimeException(String.format("BizOperationDeserializer反序列化错误：%1$s", propName));
                }
                break;
            default:
                if (!readExtendOpProperty(op, propName, jsonParser)) {
                    throw new RuntimeException(String.format("BizOperationDeserializer未识别的属性名：%1$s", propName));
                }
        }
    }

    protected boolean readExtendOpProperty(BizOperation op, String propName, JsonParser jsonParser) {
        return false;
    }

    protected abstract T createBizOp();
}
